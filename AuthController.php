<?php



// app/Http/Controllers/AuthController.php

namespace App\Http\Controllers;

// AuthController.php

// AuthController.php

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    // Metode lain yang sudah ada di sini...

    public function showRegistrationForm()
    {
        // Logika tampilan formulir registrasi
        return view('auth.register'); // Gantilah 'auth.register' dengan nama view yang sesuai
    }

    public function processRegistration(Request $request)
    {
        // Validasi data
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'gender' => 'required|string',
            'nationality' => 'required|string',
            // Atur aturan validasi sesuai kebutuhan Anda
        ]);

        // Logika pendaftaran pengguna
        // ...

        // Redirect atau berikan respons berhasil
        return redirect('/home')->with('success', 'Registrasi berhasil!');
    }
}



?>